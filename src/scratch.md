# créer un user
- sauvegarder le token
- variable
- cookie
- afficher le pseudo

# envoyer un message
- texte prédéfini
- récupérer le token
- "to = 0" pour envoyer un message public

# récupérer les messages
- récupérer le token
- afficher sur la page
- récupérer les messages à intervalles réguliers

# messages avancés 
- choisir le message à envoyer
- choisir à qui envoyer
- récupérer les utilisateurs actifs
- select/autre pour le choix du destinataire

